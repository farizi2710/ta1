<?php
use App\Http\Controllers\Users\IndexController;
use App\Http\Controllers\Users\PasswordController;
use App\Http\Controllers\Admin\HomeController;
use Illuminate\Support\Facades\Route;

Route::get('/', [IndexController::class, 'view'])->name('index')->middleware('verified');

Auth::routes(['verify' => true ]);

// routing pada Users
Route::get('logout', [IndexController::class, 'logout'])->name('logout');


// routing pada password
Route::get('user/password', [PasswordController::class, 'edit'])->name('password.edit');
Route::patch('user/password', [PasswordController::class, 'update'])->name('password.edit');


// routing pada admin
Route::get('/admin-v1', [HomeController::class, 'index'])->name('home')->middleware('verified');
Route::get('/edit/{id}', [HomeController::class, 'edit'])->name('edit');
Route::get('/View/{id}', [HomeController::class, 'show'])->name('view');
Route::post('/update/{id}', [HomeController::class, 'update'])->name('update');
Route::get('/hapus/{id}', [HomeController::class, 'delete']);




