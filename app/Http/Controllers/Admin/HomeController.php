<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $alldata = User::all();
        return view('admin.home', ['data'=>$alldata]);
    }

    public function show ($id){
        $User = User::find($id);
        return view('admin.show', ['Petani' => $User]);
    }

    public function edit ($id){
        $User = User::find($id);
        return view('admin.edit', ['Petani' => $User]);
    }

    public function update($id, Request $request)
{
 
    $User = User::find($id);
    $User->apikey = $request->apikey;
    $User->authdomain = $request->authdomain;
    $User->databaseurl = $request->databaseurl;
    $User->storagebucket = $request->storagebucket;
    $User->kebun = $request->kebun;
    $User->save();
    return redirect('admin-v1');
}
public function delete ($id){
    $User = User::find($id)->delete();
    return redirect('admin-v1');
}


}
