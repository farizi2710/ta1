<?php

namespace App\Http\Controllers\Users;
use App\Http\Controllers\Controller;
use App\Models\data;
use Illuminate\Http\Request;



class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function view()
    {
        $data = data::all();
        return view('Users.index', ['data'=>$data]);
    }

    public function logout(Request $request) {
        return redirect('/login')->with('success',"Anda Sudah Logout.");
      }


}
