@extends('Master.default')
@section('title', 'Profile User')
@section('content')
<div class="container">
    <div class="card mb-3">
        <div class="row no-gutters">
            <div class="col-md-4">
                <img src="/images/sas.png" alt="..." width="100%">
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h4>Profile Users</h4></div>
                    <div class="card-body">
                        <form>
                            <div class="form-group row">
                              <label for="staticEmail" class="col-sm-2 col-form-label">Name Garden</label>
                              <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext" value="{{$Petani->name}}">
                              </div>
                            </div>
    
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-8">
                                  <input type="text" readonly class="form-control-plaintext" value="{{ $Petani->email }}"> <a href="https://mail.google.com/mail/?view=cm&fs=1&to={{ $Petani->email }}">kirim</a>
                                </div>
                            </div>

                           @if ($Petani->apikey != "nothing")
                               <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">verified at</label>
                                    <div class="col-sm-8">
                                      <input type="text" readonly class="form-control-plaintext" value=" Akun terverifikasi" style="color:blue ;" >
                                    </div>
                                </div>
                            @else
                               <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">verified at</label>
                                    <div class="col-sm-8">
                                      <input type="text" readonly class="form-control-plaintext" value=" Akun belum terverifikasi" style="color:red ;" >
                                    </div>
                                </div>
                           @endif
    
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Created at</label>
                                <div class="col-sm-8">
                                  <input type="text" readonly class="form-control-plaintext" value="{{ $Petani->created_at->format("d, F Y  H:i:A")}}">
                                </div>
                            </div>
    
    
                           @if ($Petani->apikey != "nothing")
                               <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">Status Kebun</label>
                                    <div class="col-sm-8">
                                      <input type="text" readonly class="form-control-plaintext" value="Aktif / Terverifikasi" style="color:blue ;" >
                                    </div>
                                </div>
                            @else
                               <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">Status Kebun</label>
                                    <div class="col-sm-8">
                                      <input type="text" readonly class="form-control-plaintext" value="Tidak Aktif / Tidak Terverifikasi" style="color:red ;" >
                                    </div>
                                </div>
                           @endif

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
