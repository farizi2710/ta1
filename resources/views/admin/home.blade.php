@extends('Master.default')
@section('title', 'Halaman Admin')
@section('content')
@if(Auth::user()->email =='farizi2710@gmail.com' || Auth::user()->email =='farizi271099@gmail.com' )
<div class="container">
        <h1 class="text-center" style="font-family: 'Staatliches', cursive;"> Daftar User</h1>
        {{-- @if ($petani->name =='customer1' || $petani->name =='admin' )
        @else --}}
            <div class="row" align="center">
                @foreach ($data as $petani)
                @if ($petani->name =='customer1' || $petani->name =='admin' )
                @else
                    <div class="col-md-6 mt-3">
                        <div class="card" style="width: 30rem;">
                            <div class="card-header d-flex justify-content-between">
                                {{$petani->name}} <span class="material-icons md-18">person</span>
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item d-flex justify-content-between"><b>Email  </b>  {{$petani->email}}  </li>
                                <li class="list-group-item d-flex justify-content-between"><b>Created</b> {{$petani->created_at->format("d, F Y")}}   </li>
                                @if ($petani->apikey != "nothing")
                                <li class="list-group-item d-flex justify-content-between"><b>Status</b><p style="color: blue">Kebun aktif</p> </li>   
                                @else
                                <li class="list-group-item d-flex justify-content-between"><b>Status</b> <p style="color: red">Kebun tidak aktif <a href="/edit/{{$petani->id}}">aktifkan</a> </p></li>
                                @endif  
                                @if ($petani->kebun > 0)
                                <li class="list-group-item d-flex justify-content-between"><b>Jumlah Kebun</b>  {{$petani->kebun}} kebun</li>
                                @else
                                <li class="list-group-item d-flex justify-content-between"><b>Jumlah Kebun</b>  {{$petani->kebun}} Tidak ada kebun</li>
                                @endif 

                            </ul>
                            <div class="card-body d-flex justify-content-between">
                                <a href="/View/{{$petani->id}}" class="btn btn-sm btn-primary">View</a>
                                <a href="/edit/{{$petani->id}}" class="btn btn-sm btn-warning">Edit</a>
                                <a href="/hapus/{{$petani->id}}" class="btn btn-sm btn-danger" onclick="Swal.fire("Oke", "Data Berhasil Di Tambah!", "success");">Delete</a>
                            </div>
                            
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
    </div>
</div>
@else
<div class="container text-center">
    <div style="font-family: 'Staatliches', cursive;">
        <h1 style="font-size: 92px">-404-</h1>
        <hr>
        <h1 style="">Sorry</h1>
        <H1>Ooops Something Went Wrong</H1>
        <H1>It's admin page, You're not admin. please, go back </H1>
    </div>
    <h4><a class="btn btn-primary" href="/">Back</a> or <a href="href="{{ route('logout') }}"
        onclick="event.preventDefault();
    document.getElementById('logout-form').submit();" class="btn btn-danger">Logout for {{Auth::user()->email}} </a></h4>
</div>
@endif
@endsection

        {{-- <table class="table table-bordered"  id="example">
            <tr>
                <th>Nama Akun</th>
                <th>Jumlah Kebun</th>
                <th>terverifikasi</th>
                <th>Tanggal Pembuatan</th>
                <th>Kode Api Key</th>
                <th>diaktifkan tanggal</th>
                <th>aksi</th>
            </tr>
            @foreach ($data as $petani)
            <tbody>
            <tr>
                <td>{{$petani->name}} </td>
                <td>{{$petani->kebun}} </td>
                <td>{{$petani->email_verified_at}}</td>
                    <td>{{$petani->created_at->format("d, F Y H:i") }} || {{$petani->created_at->format("H:i")}} WIB</td>
                    <td>{{$petani->apikey}} </td>
                    <td>{{$petani->updated_at->format("d, F Y")}} || {{$petani->updated_at->format("H:i")}} WIB</td>
                <td>
                    <a href="/edit/{{$petani->id}}" class="btn btn-primary m-2">Edit</a>
                    <a href="/View/{{$petani->id}}" class="btn btn-danger  m-2">Lihat</a>
                    <a href="/hapus/{{$petani->id}}" class="btn btn-danger  m-2">Delete</a>
                </td>
            </tr>
            </tbody> 
        </table> --}}