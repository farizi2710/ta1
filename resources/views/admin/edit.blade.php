@extends('Master.default')
@section('title', 'Edit User')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">
                    <form method="POST" action="/update/{{ $Petani->id }}">
                        @csrf
                        <h4>Firebase Konfigurasi</h4>
                        <hr>
                        <div class="form-group row">
                            <label for="apikey" class="col-md-4 col-form-label text-md-right">Api Key</label>
                            <div class="col-md-6">
                                <select class="form-control @error('apikey') is-invalid @enderror" name="apikey" value="{{ $Petani->apikey }}" required autocomplete="apikey" autofocus>
                                    <option>{{ $Petani->apikey }}</option>
                                    <option>AIzaSyCgvGqsBktM53vKvi8DejqG1pOGDl4QN0U</option>
                                    <option>nothing</option>
                                </select>
                                @error('apikey')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="authdomain" class="col-md-4 col-form-label text-md-right">Auth Domain</label>

                            <div class="col-md-6">
                                <select class="form-control @error('authdomain') is-invalid @enderror" name="authdomain"required autocomplete="authdomain" autofocus>
                                    <option>{{$Petani->authdomain}}</option>
                                    <option>tafarizi.firebaseapp.com</option>
                                </select>
                                @error('authdomain')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="databaseurl" class="col-md-4 col-form-label text-md-right">Database Url</label>

                            <div class="col-md-6">
                                <select class="form-control @error('databaseurl') is-invalid @enderror" name="databaseurl"  required autocomplete="databaseurl" autofocus>
                                    <option>{{ $Petani->databaseurl }}</option>
                                    <option>https://tafarizi-default-rtdb.firebaseio.com</option>
                                </select>
                                @error('databaseurl')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="storagebucket" class="col-md-4 col-form-label text-md-right">Storage Bucket</label>

                            <div class="col-md-6">
                                <select class="form-control @error('storagebucket') is-invalid @enderror" name="storagebucket"  required autocomplete="storagebucket" autofocus>
                                    <option>{{ $Petani->storagebucket }}</option>
                                    <option>tafarizi.appspot.com</option>
                                </select>
                                @error('storagebucket')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="kebun" class="col-md-4 col-form-label text-md-right">Storage Bucket</label>

                            <div class="col-md-6">
                                <select class="form-control @error('kebun') is-invalid @enderror" name="kebun"  required autocomplete="kebun" autofocus>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                </select>
                                @error('kebun')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Edit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
