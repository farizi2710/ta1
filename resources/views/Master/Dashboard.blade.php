<!doctype html>
<html lang="id">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <title>@yield('title','Dahsboard')</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                @if(Auth::user()->email =='farizi2710@gmail.com' || Auth::user()->email =='farizi271099@gmail.com' )
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('home') }}">Dashboard Admin</a>
                        </li>
                    </ul>
                @else
                    
                @endif

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest  
                        @if (Route::has('login'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @endif
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->email }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                  document.getElementById('logout-form').submit();">
                                    <span class="material-icons" style="size: 3px">
                                        logout
                                    </span>
                                     logout
                                 </a>

                                <a class="dropdown-item btn btn-info" data-toggle="modal" data-target="#view">
                                <span class="material-icons">
                                    error_outline
                                    </span>
                                     Profile
                                 </a>

                                 <a class="dropdown-item btn btn-info" href="{{ route('password.edit') }}">
                                    <span class="material-icons">
                                        manage_accounts
                                    </span>
                                    Change Password
                                 </a>
                                @if (Auth::user()->email =='farizi2710@gmail.com')
                                <a class="dropdown-item" href="{{ route('home') }}">
                                    <span class="material-icons">
                                        account_circle
                                    </span>
                                    halaman admin
                                </a>
                                @endif

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>                    
@yield('konten')


<script src="https://code.jquery.com/jquery-3.4.0.min.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.10.1/firebase.js"></script>
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.all.min.js"></script>

<style>
    @media screen and (min-width: 601px) {
  h1,h2,h3,h4,h6 {
    font-size: 30px;
  }
}

@media screen and (max-width: 600px) {
  h1,h2,h3,h4,h6 {
    font-size: 20px;
  }
}
</style>
<script>
    // Initialize Firebase
    var config = {
        apiKey:         "{{ Auth::user()->apikey }}",
        authDomain:     "{{ Auth::user()->authdomain }}",
        databaseURL:    "{{ Auth::user()->databaseurl }}",
        storageBucket:  "{{ Auth::user()->storageurl }}",
    };
    firebase.initializeApp(config);
    var database = firebase.database();
    var lastIndex = 0;
    // Get Data
    firebase.database().ref('kebun/{{ Auth::user()->name }}/').on('value', function (snapshot) {
        var value = snapshot.val();
        var htmls = [];
        $.each(value, function (index, value) {
            if (value) {
            htmls.push('<hr>\
			<div class="d-flex justify-content-between mt-4">\
				<h4><span class="material-icons">local_florist</span>' + value.lokasi + '</h4>\
				<h4><span class="material-icons">gps_fixed</span>' + value.tempat + '</h4>\
                <h4><span class="material-icons">gps_fixed</span> kebun ' +  index + '</h4>\
			</div>\
			<hr>\
            <div class ="row">\
                <div class="col-md-3 mt-5 " >\
					<div class="card text-center">\
					  <h5 class="card-header bg-warning">Temperature</h5>\
					  <div class="card-body bg-dark text-light">\
					    <H1 class="card-title">' + value.suhu + '°C</H1>\
					  </div>\
					</div>\
				</div>\
                <div class="col-md-3 mt-5" >\
					<div class="card text-center">\
					  <h5 class="card-header bg-warning">Kelembapan Tanah</h5>\
					  <div class="card-body bg-dark text-light">\
					    <H1 class="card-title">' + value.tanah1+ '%</H1>\
					  </div>\
					</div>\
				</div>\
				<div class="col-md-3 mt-5 " >\
					<div class="card text-center">\
					  <h5 class="card-header bg-warning">Status Pompa</h5>\
					  <div class="card-body bg-dark text-light">\
					    <H1 class="card-title">' + value.status+ '</H1>\
					  </div>\
					</div>\
				</div>\
				<div class="col-md-3 mt-5" >\
					<div class="card text-center">\
					  <h5 class="card-header bg-warning">last update</h5>\
					  <div class="card-body bg-dark text-light">\
					    <H1 class="card-title">' + value.timenow + ' WIB</H1>\
					  </div>\
					</div>\
				</div>\
            </div>\
                <div class ="row">\
                    <div class="col-md-12 mt-4">\
                    <div class="card text-center">\
                        <h5 class="card-header bg-warning">Aksi</h5>\
                        <div class="card-body bg-dark text-light">\
                                <button data-toggle="modal" data-target="#update-modal" class="btn btn-info updateData mr-3" data-id="' + index + '"><span class="material-icons">update</span>Update</button>\
                                <button data-toggle="modal" data-target="#remove-modal" class="btn btn-danger removeData" data-id="' + index + '"><span class="material-icons">delete</span>Delete</button>\
                                <hr>\
                                <button data-toggle="modal" data-target="#update-modal-on" class="btn btn-info updateDataon mr-3 " data-id="' + index + '"><span class="material-icons">power_settings_new</span>TurnON</button>\
                                <button data-toggle="modal" data-target="#update-modal-off" class="btn btn-danger updateDataoff " data-id="' + index + '"><span class="material-icons">power_settings_new</span>TurnOFF</button>\
                                <button data-toggle="modal" data-target="#update-modal-off" class="btn btn-success updateDataoff ml-3 " data-id="' + index + '"><span class="material-icons">timer</span>Atur Waktu</button>\
                            </div>\
                        </div>\
                        <div class="card-footer">\
                            <div class="d-flex justify-content-between">\
                                <p style="font-size : 30 px ;">Status Pompa : <B>' + value.status + '</b></p>\
                                <p style="font-size : 30 px ;">LastUpdate : <B>' + value.timenow + '</b></p>\
                                <p style="font-size : 30 px ;">Watering At : <B>' + value.rtc + '</b></p>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
        	');
            }
            lastIndex = index;
        });
        $('#tbody').html(htmls);
        $("#submitUser").removeClass('desabled'); 
    });
    // Add Data
        $('#submitkebun').on('click', function () {
        var values  = $("#tambahkankebun").serializeArray();
        var lokasi  = values[0].value;
        var tempat  = values[1].value;
        var suhu    = values[2].value;
        var tanah1  = values[3].value;
        var tanah2  = values[4].value;
        var tanah3  = values[5].value;
        var status  = values[6].value;
        var rtc     = values[7].value;
        var rtc2     = values[8].value;
        var userID  = lastIndex + 1;
        alert('suhu');
        console.log(values);
        console.log(suhu);
        if (userID <= {{ Auth::user()->kebun }}) {
            firebase.database().ref('kebun/{{ Auth::user()->name }}/' + userID).set({
            lokasi: lokasi,
            tempat: tempat,
            suhu: suhu,
            tanah1: tanah1,
            tanah2: tanah2,
            tanah3: tanah3,
            status: status,
            rtc: rtc,
            rtc2: rtc2,
            
        });
        Swal.fire("Oke", "Data Berhasil Di Tambah!", "success");
        }
        else{
            alert('mohon maaf {{ Auth::user()->email }} anda hanya punya {{ Auth::user()->kebun }} kebun')
        }
        
        // Reassign lastID value
        lastIndex = userID;
        
        $("#tambahkankebun input").val("");
        });




    // Update Data
    var updateID = 0;
    $('body').on('click', '.updateData', function () {
        updateID = $(this).attr('data-id');
        firebase.database().ref('kebun/{{ Auth::user()->name }}/' + updateID).on('value', function (snapshot) {
            var values = snapshot.val();
            var updateData = '<div class="form-group">\
		        <label for="lokasi" class="col-md-12 col-form-label">lokasi</label>\
		        <div class="col-md-12">\
		            <input id="lokasi" type="text" class="form-control" value="' + values.lokasi + '" name="lokasi" required>\
		        </div>\
		    </div>\
		    <div class="form-group">\
		        <label for="tempat" class="col-md-12 col-form-label">tempat</label>\
		        <div class="col-md-12">\
		            <input id="tempat" type="text" class="form-control" name="tempat" value="' + values.tempat + '" required>\
		        </div>\
            </div>\
		            <input id="suhu" type="text" class="form-control" name="suhu" value="' + values.suhu + '" hidden>\
		            <input id="tanah1" type="text" class="form-control" name="tanah1" value="' + values.tanah1 + '" hidden>\
		            <input id="tanah2" type="text" class="form-control" name="tanah2" value="0" hidden>\
                    <input id="tanah3" type="text" class="form-control" name="tanah3" value="0" hidden>\
                    <input id="status" type="text" class="form-control" name="status" value="OFF" hidden>\
            <div class="form-group">\
		        <label for="status" class="col-md-12 col-form-label">Watering</label>\
		        <div class="col-md-12">\
		            <input type="text" name="rtc" class="form-control" value="' + values.rtc + '">\
		        </div>\
            <div class="form-group">\
		        <label for="status" class="col-md-12 col-form-label">Prose Penyiraman</label>\
                <div class="col-md-12 d-flex justify-content-between">\
                    <input id="delay" type="number" class="form-control" name="delay" placeholder="minutes" require>menit\
                </div>\
		    </div>';

            $('#updateBody').html(updateData);
        });
    });

    $('.update').on('click', function () {
        var values = $(".users-update-record-model").serializeArray();
        var postData = {
            lokasi: values[0].value,
            tempat: values[1].value,
            suhu:   values[2].value,
            tanah1: values[3].value,
            tanah2: values[4].value,
            tanah3: values[5].value,
            status: values[6].value,
            rtc:    values[7].value,
            delay:    parseInt(values[8].value * 60000),
        };
            var updates = {};
            updates['/kebun/{{ Auth::user()->name }}/' + updateID] = postData;

            firebase.database().ref().update(updates);
            Swal.fire("Oke", "Data Berhasil Di Update! Penyiraman akan", "success");
            $("#update-modal").modal('hide');
    });
    


    // Remove Data
    $("body").on('click', '.removeData', function () {
        var id = $(this).attr('data-id');
        $('body').find('.users-remove-record-model').append('<input name="id" type="hidden" value="' + id + '">');
    });

    $('.deleteRecord').on('click', function () {
        var values = $(".users-remove-record-model").serializeArray();
        var id = values[0].value;
        firebase.database().ref('kebun/{{ Auth::user()->name }}/' + id).remove();
        $('body').find('.users-remove-record-model').find("input").remove();
        $("#remove-modal").modal('hide');
        Swal.fire("Oke", "Kebun Di Hapus!", "success").then(function(){ 
        location.reload();
   }
);;
    });
    $('.remove-data-from-delete-form').click(function () {
        $('body').find('.users-remove-record-model').find("input").remove();
    });

// on
var updateonID = 0;
    $('body').on('click', '.updateDataon', function () {
        updateonID = $(this).attr('data-id');
        firebase.database().ref('kebun/{{ Auth::user()->name }}/' + updateonID).on('value', function (snapshot) {
            var values = snapshot.val();
            var updateDataon = '<h3>Apakah Anda Yakin <b> Hidupkan Pompa?</b></h3>\
		            <input id="lokasi" type="text" class="form-control" name="lokasi" value="' + values.lokasi + '" required hidden>\
		            <input id="tempat" type="text" class="form-control" name="tempat" value="' + values.tempat + '" required hidden>\
		            <input id="suhu" type="text" class="form-control" name="suhu" value="' + values.suhu + '" hidden>\
		            <input id="tanah1" type="text" class="form-control" name="tanah1" value="' + values.tanah1 + '" hidden>\
		            <input id="tanah2" type="text" class="form-control" name="tanah2" value="' + values.tanah2 + '" hidden>\
		            <input id="tanah3" type="text" class="form-control" name="tanah3" value="' + values.tanah3 + '" hidden>\
                    <input id="status" type="text" class="form-control" name="status" value="ON" hidden>\
                    <input id="rtc" type="text" class="form-control" name="rtc" value="' + values.rtc + '" hidden>\
                    <input id="delay" type="number" class="form-control" name="number" value="' + values.delay + '" hidden>';      
            $('#updateBodyon').html(updateDataon);
        });
    });

    var updateoffID = 0;
    $('body').on('click', '.updateDataoff', function () {
        updateoffID = $(this).attr('data-id');
        firebase.database().ref('kebun/{{ Auth::user()->name }}/' + updateoffID).on('value', function (snapshot) {
            var values = snapshot.val();
            var updateDataoff = '<h3>Apakah Anda Yakin <b> Matikan Pompa?</b></h3>\
		            <input id="lokasi" type="text" class="form-control" name="lokasi" value="' + values.lokasi + '" required hidden>\
		            <input id="tempat" type="text" class="form-control" name="tempat" value="' + values.tempat + '" required hidden>\
		            <input id="suhu" type="text" class="form-control" name="suhu" value="' + values.suhu + '" hidden>\
		            <input id="tanah1" type="text" class="form-control" name="tanah1" value="' + values.tanah1 + '" hidden>\
		            <input id="tanah2" type="text" class="form-control" name="tanah2" value="' + values.tanah2 + '" hidden>\
		            <input id="tanah3" type="text" class="form-control" name="tanah3" value="' + values.tanah3 + '" hidden>\
                    <input id="status" type="text" class="form-control" name="status" value="OFF" hidden>\
                    <input id="rtc" type="text" class="form-control" name="rtc" value="' + values.rtc + '" hidden>\
                    <input id="delay" type="number" class="form-control" name="number" value="' + values.delay + '" hidden>';

            $('#updateBodyoff').html(updateDataoff);
        });
    });
    $('.updateon').on('click', function () {
        var values = $(".users-updateon-record-model").serializeArray();
        var postData = {
            lokasi  : values[0].value,
            tempat  : values[1].value,
            suhu    : values[2].value,
            tanah1  : values[3].value,
            tanah2  : values[4].value,
            tanah3  : values[5].value,
            status  : values[6].value,
            rtc     : values[7].value,
            delay:    parseInt(values[8].value),
        };

        var updates = {};
        updates['/kebun/{{ Auth::user()->name }}/' + updateonID] = postData;

        firebase.database().ref().update(updates);

        $("#update-modal-on").modal('hide');
        Swal.fire("Oke" , "Pompa Dihidupkan!", "success");
    });


    $('.updateoff').on('click', function () {
        var values = $(".users-updateoff-record-model").serializeArray();
        var postData = {
            lokasi: values[0].value,
            tempat: values[1].value,
            suhu:   values[2].value,
            tanah1: values[3].value,
            tanah2: values[4].value,
            tanah3: values[5].value,
            status: values[6].value,
            rtc:    values[7].value,
            delay:    parseInt(values[8].value),
        };

        var updates = {};
        updates['/kebun/{{ Auth::user()->name }}/' + updateoffID] = postData;

        firebase.database().ref().update(updates);

        $("#update-modal-off").modal('hide');
        Swal.fire("Oke", "Pompa Dimatikan!", "success");
    });

</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>