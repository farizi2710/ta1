@extends('Master.login')
@section('title', 'Halaman Register')
@section('content')
    @if(session('success'))
                <div class="alert alert-success" role="alert">
                    {{session('success')}}
                </div>
    @endif
	<div class="limiter ">
		<div class="container-login100" style="background-image: url('/images/bg-01.jpg');">
			<div class="wrap-login100 p-t-30 p-b-50">
				<span class="login100-form-title p-b-41">
					Account Register
				</span>
                <form class="login100-form validate-form p-b-33 p-t-5" method="POST" action="{{ route('register') }}">
                    @csrf
                    {{-- user name --}}
                    <div class="wrap-input100 validate-input" data-validate  = "Enter name"  for="name" >
						<input class="input100 @error('name') is-invalid @enderror" type="name" name="name" placeholder="Insert Your Garden Name" value="{{ old('name') }}">
                        <span class="focus-input100" data-placeholder="&#xe849;">
                        </span>
                    </div>
                    {{-- email --}}
                    <div class="wrap-input100 validate-input" data-validate  = "Enter Email"  for="email" >
						<input class="input100 @error('email') is-invalid @enderror" type="email" name="email" placeholder="Insert Your Email" value="{{ old('email') }}">
                        <span class="focus-input100" data-placeholder="&#xe82a;">
                            @error('email')
                            <div class="container">
                                <strong style="color: red">{{ $message }}</strong>
                            </div>
                            @enderror
                        </span>
                    </div>
                    {{-- firebase --}}
                        <input id="apikey" type="text" class="form-control @error('apikey') is-invalid @enderror" name="apikey" value="nothing" required autocomplete="apikey" autofocus hidden>
                        <input id="authdomain" type="text" class="form-control @error('authdomain') is-invalid @enderror" name="authdomain" value="nothing" required autocomplete="authdomain" autofocus hidden>
                        <input id="databaseurl" type="text" class="form-control @error('databaseurl') is-invalid @enderror" name="databaseurl" value="nothing" required autocomplete="databaseurl" autofocus hidden>
                        <input id="storagebucket" type="text" class="form-control @error('storagebucket') is-invalid @enderror" name="storagebucket" value="nothing" required autocomplete="storagebucket" autofocus hidden>
                    {{-- <div class="wrap-input100 validate-input" data-validate="Enter avatar" for="avatar">
                        <input class="input100 @error('avatar') is-invalid @enderror" type="file" name="avatar" placeholder="avatar"  autocomplete="new-avatar">
                        <span class="focus-input100" data-placeholder="&#xe80f;">
                            @error('avatar')
                            <div class="container">
                                <strong>{{ $message }}</strong>
                            </div>
                                @enderror
                        </span>
                    </div> --}}
                    {{-- p --}}
					<div class="wrap-input100 validate-input" data-validate="Enter password" for="password">
						<input class="input100 @error('password') is-invalid @enderror" type="password" name="password" placeholder="Password"  autocomplete="new-password">
                        <span class="focus-input100" data-placeholder="&#xe80f;">
                            @error('password')
                            <div class="container">
                                <strong>{{ $message }}</strong>
                            </div>
                             @enderror
                        </span>
                    </div>
                    
                    <div class="wrap-input100 validate-input" data-validate="Enter password" for="password-confirm">
						<input class="input100 @error('password') is-invalid @enderror" type="password" name="password_confirmation" placeholder="Repeat Your Password !" id="password-confirm" autocomplete="new-password" >
                        <span class="focus-input100" data-placeholder="&#xe862;">
                            @error('password')
                            <div class="container">
                                <strong>{{ $message }}</strong>
                            </div>
                             @enderror
                        </span>
                    </div>
                


					<div class="container-login100-form-btn m-t-32">
						<button class="login100-form-btn" type="submit">
							Register
                        </button>
                    </div>

                    <div class="container-login100-form-btn m-t-32">
							<a href="/" style="color: black;">Kembali</a>
                    </div>
                           
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
@endsection
