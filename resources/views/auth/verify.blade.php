@extends('Master.login')
@section('title', 'Halaman Verifikasi')
@section('content')
<div class="limiter ">
    <div class="container-login100 text-center" style="background-image: url('/images/bg-03.jpg');">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex align-items-center justify-content-between">
                    <div>
                        Silahkan Verifikasi email anda
                    </div>
                    <div>
                        {{ auth::user()->email}}
                    </div>
                </div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif
                    Sebelum melanjutkan, periksa email Anda untuk tautan verifikasi. 
                    Jika Anda tidak menerima email,
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">klik disini</button>.
                    </form>
                </div>
                @guest
                    
                @else

                <div class="card-footer">
                    <a class="btn btn-primary" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                        {{ __('Logout') }} {{ auth::user()->email}}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
                @endguest
            </div>
        </div>
    </div>
</div>
<div id="dropDownSelect1"></div>
@endsection
