@extends('Master.login')
@section('title', 'Halaman Login')
@section('content')
        <div class="limiter ">
            <div class="container-login100" style="background-image: url('/images/bg-03.jpg');">
                <div class="wrap-login100 p-t-20 p-b-50">
                    <h1 class="login100-form-title p-b-10">
                        Login For Acsses your garden
                    </h1>
                    <div class="container text-center mb-3">
                        <h3><a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">Login</a></h3>
                    </div>
                    <div class="collapse" id="collapseExample">
                        <form class="login100-form validate-form p-b-33 p-t-5" method="POST" action="{{ route('login') }}">
                            @csrf
                            <h4 class="text-center">Login Form</h4>
                            <hr>
                            <div class="wrap-input100 validate-input" data-validate="Enter Email"  for="email"  >
                                <input class="input100 @error('email') is-invalid @enderror text-warning" type="email" name="email" placeholder="Email" value="{{ old('email') }}">
                                <span class="focus-input100" data-placeholder="&#xe82a;">
                                    @error('email')
                                    <div class="container">
                                        <strong style="color: red">{{ $message }}</strong>
                                    </div>
                                    @enderror
                                </span>
                            </div>
                            
        
                            <div class="wrap-input100 validate-input" data-validate="Enter password" for="password">
                                <input class="input100 @error('password') is-invalid @enderror text-warning" type="password" name="password" placeholder="Password" id="password" >
                                <span class="focus-input100" data-placeholder="&#xe80f;">
                                    @error('password')
                                    <div class="container">
                                        <strong>{{ $message }}</strong>
                                    </div>
                                    @enderror
                                </span>
                            
                                
                            </div>
        
                            <div class="container-login100-form-btn m-t-32">
                                <button class="login100-form-btn" type="submit">
                                    Login
                                </button>
                                @if (Route::has('password.request'))
                                <a class="btn btn-link text-warning" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                                @endif
                                
                            </div>
                            
                            <div class="container-login100-form-btn mt-2">
                                <h4>-- or --</h4>
                            </div>
                            <div class="container-login100-form-btn mt-2">
                                @if (Route::has('register'))
                                    <a class="register100-form-btn" href="{{ route('register') }}">Sign-Up</a>
                                @endif
                            </div>     
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div id="dropDownSelect1"></div>
        @push('script')
        <script>
            new Typed('#typed',{
            strings : ['HTML','CSS','BOOTSTRAP','JAVASCRIPT'],
            typeSpeed : 100,
            delaySpeed : 100,
            loop : true
            });
        </script>
            
        @endpush
@endsection
