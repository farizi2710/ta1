@extends('master.Dashboard')
@extends('Users.komponen.card')
@extends('Users.komponen.modal')
@section('title','Halaman Awal')
@section('konten')
@section('card')
@section('modal')
<div class="container" style="margin-top: 50px;">
@if (Auth::user()->apikey !='nothing' || Auth::user()->email =='farizi2710@gmail.com' || Auth::user()->email =='farizi271099@gmail.com')
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Hello admin!</strong> Welcome, Any Problems with your Garden ? <a href="https://wa.me/6282229414052?text=Assalamualaikum%20mas%20Fariz%2C%20Saya%20dari%20kebun%20{{ Auth::user()->name }},%20{{ Auth::user()->email }}%20Mendaftar tanggal%20{{ Auth::user()->created_at->format("d, F Y H:i ") }}%20untuk%20menanyakan%20kenapa%20ya%20mas%20kebun%20saya%20tidak%20terdeteksi%3F">Please Contact us </a>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <h4 class="text-center">Temperature and Moisture Monitoring System Based on Internet of Things</h4><br>
    <p><a class="btn btn-primary text-center" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample"><span class="material-icons">add_location</span>Tambahkan Kebun</a></p>
    <div class="collapse bg-warning" id="collapseExample">
        <div class="card card-default">
        <div class="card-header mt-2">Tambah Kebun</div>
        <div class="card-body">
            <form id="tambahkankebun" method="POST" action="">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="lokasi">Lokasi</label>
                            <input id="lokasi" type="text" class="form-control" name="lokasi" placeholder=" Example : Jakarta, Surabaya etc.." required>
                        </div>
                        <div class="form-group">
                            <label for="tempat">tempat</label>
                            <input id="tempat" type="text" class="form-control" name="lokasi" placeholder="Example : Dapur, Ruang Tengah , Teras depan etc.." required>
                        </div>
                    </div>
                    <input id="suhu" type="email" class="form-control"  name="suhu"     value="0"   placeholder="suhu"  hidden >
                    <input id="tanah1" type="text" class="form-control" name="tanah1"   value="0"   placeholder="tanah1"  hidden >
                    <input id="tanah2" type="number" class="form-control" name="tanah2"  placeholder="tanah2"  hidden >
                    <input id="tanah3" type="text" class="form-control" name="tanah3"   value="0"   placeholder="tanah3"  hidden >
                    <input id="status" type="text" class="form-control" name="status"   value="off"   placeholder="status"  value="OFF" hidden>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="rtc">Watering 1</label>
                            <input class="form-control" name="rtc" type="text" required>
                            <small>contoh : 9:0 (9 pagi) & 13.0(1 siang) / 10.30 /13.30  </small>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="rtc">Watering 2 </label>
                            <div class="form-group">
                                <label for="rtc">Watering 1</label>
                                <input class="form-control" name="rtc" type="text" required>
                                <small>contoh : 9:0 (9 pagi) & 13.0(1 siang) / 10.30 /13.30  </small>
                            </div>
                        </div>
                        <button id="submitkebun" type="button" class="btn btn-primary mb-2"><span class="material-icons">send</span>Submit</button>
                    </div>
                </div>
            </form>
        </div>   
    </div>
</div>
<hr>
<h3 class="text-secondary text-center">kebun Mu {{Auth::user()->name}}</h3>
<hr>
<div class="container" style="margin-top: 20px" id="tbody">

</div>

<div class="row" style="margin-top: 20px" id="card">

</div>
    @else
    <div class="container">
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading text-center">Something Wrong! </h4>
            <hr>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h6 class="text-center">Dear Customer <em>"{{ auth::user()->email}}"</em> Don't have garden? Please Purchase our Product :)</h6>
            <hr>
            <h6 class="mb-0  text-center"> Silahkan Hubungi Kami <a href="https://wa.me/6282229414052?text=Assalamualaikum%20mas%20Fariz%2C%20Saya%20dari%20kebun%20{{ Auth::user()->name }},%20{{ Auth::user()->email }}%20Mendaftar tanggal%20{{ Auth::user()->created_at->format("d, F Y H:i ") }}%20untuk%20ingin%20menanyakan%20apa%20produk%20tersedia?%20">Chat Via WhatsApp</a></h6>
          </div>
        <div class="container m-8" align="center">
            <hr>
            <h1>Portofolio</h1>
            <hr>
            <div class="row">
                <div class="col-md-4 mt-2">
                    <div class="card" style="width: 18rem;">
                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRe4Ph4d_2Q3qrAZlpp3R6nqkEasZabzsfsUQ&usqp=CAU" class="card-img-top" alt="..." style=" height: 250px ; object-fit:cover; object-position: center;">
                        <div class="card-body">
                        <h5 class="card-title">Jasa Pembuatan Website</h5>
                        <p class="card-text">Website dengan segala urusan mulai dari individu website sampai perusahan</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 mt-2">
                    <div class="card" style="width: 18rem;">
                        <img src="https://images.idgesg.net/images/article/2020/08/android-awkward-timing-100855433-large.jpg" class="card-img-top" alt="" style=" height: 250px ; object-fit:cover; object-position: center;">
                        <div class="card-body">
                        <h5 class="card-title">Jasa Pembuatan Aplikasi</h5>
                        <p class="card-text">aplikasi dengan segala urusan mulai dari individu website sampai perusahan</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 mt-2">
                    <div class="card" style="width: 18rem;">
                        <img src="https://i.pinimg.com/originals/03/99/ba/0399ba6e400f6b6f16f78d42a74cc1bc.png" class="card-img-top" alt="..." style=" height: 250px ; object-fit:cover; object-position: center;">
                        <div class="card-body">
                        <h5 class="card-title">Monitoring Kebun</h5>
                        <p class="card-text">Asisten pribadi anda untuk monitoring kebun dan juga melakukan penyiraman otomatis</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
            </div>
            
        </div> 
    </div> 
    @endif
@endsection



