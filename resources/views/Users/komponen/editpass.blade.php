@extends('Master.default')
@section('title', 'Edit Password')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Ganti Password</div>
                <div class="card-body">
                    <form action="{{route('password.edit')}}" method="POST">
                        @csrf
                        @method("PATCH")
                        <div class="form-group">
                            <label for="old-password">Old Password</label>
                            <input type="password" name="old-password" id="old_password" class="form-control">
                            @error('old_password')
                                <div class="text-danger-m-2">{{$message}}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="password">New Password</label>
                            <input type="password" name="password" id="password" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="password-comfirmation">Komfirmasi Password</label>
                            <input type="password-comfirmation" name="password-comfirmation" id="password-comfirmation" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-primary">Change Password</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
