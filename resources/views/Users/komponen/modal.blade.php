    
@yield('modal')
<!-- Update Model -->
<div class="container" align="center">
    <form action="" method="POST" class="users-update-record-model form-horizontal">
        <div id="update-modal" data-backdrop="static" data-keyboard="false" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" style="width:100%">
                <div class="modal-content" style="overflow: hidden;">
                    <div class="modal-header">
                        <h4 class="modal-title" id="custom-width-modalLabel">Update</h4>
                        <button type="button" class="close" data-dismiss="modal"
                                aria-hidden="true">×
                        </button>
                    </div>
                    <div class="modal-body" id="updateBody">
    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                                data-dismiss="modal">Close
                        </button>
                        <button type="button" class="btn btn-success update">Update
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

{{-- // update on --}}
<div class="container" align="center">
    <form action="" method="POST" class="users-updateon-record-model form-horizontal">
        <div id="update-modal-on" data-backdrop="static" data-keyboard="false" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" style="width:100%">
                <div class="modal-content" style="overflow: hidden;">
                    <div class="modal-header">
                        <h4 class="modal-title" id="custom-width-modalLabel">Update Pompa</h4>
                        <button type="button" class="close" data-dismiss="modal"
                                aria-hidden="true">×
                        </button>
                    </div>
                    <div class="modal-body" id="updateBodyon">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                                data-dismiss="modal">Gak Jadi Deh
                        </button>
                        <button type="button" class="btn btn-success updateon">Hidupkan Pompa
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

{{-- // update off --}}
<div class="container" align="center">
    <form action="" method="POST" class="users-updateoff-record-model form-horizontal">
        <div id="update-modal-off" data-backdrop="static" data-keyboard="false" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" style="width:100%">
                <div class="modal-content" style="overflow: hidden;">
                    <div class="modal-header">
                        <h4 class="modal-title" id="custom-width-modalLabel">Update Pompa</h4>
                        <button type="button" class="close" data-dismiss="modal"
                                aria-hidden="true">×
                        </button>
                    </div>
                    <div class="modal-body" id="updateBodyoff">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                                data-dismiss="modal">Gak Jadi Deh
                        </button>
                        <button type="button" class="btn btn-success updateoff">Matikan Pompa
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>



<!-- Delete Model -->
<form action="" method="POST" class="users-remove-record-model">
    <div id="remove-modal" data-backdrop="static" data-keyboard="false" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-dialog-centered" style="width:100%">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="custom-width-modalLabel">Delete</h4>
                    <button type="button" class="close remove-data-from-delete-form" data-dismiss="modal"
                            aria-hidden="true">×
                    </button>
                </div>
                <div class="modal-body">
                    <p>Apakah ingin Menghapus Kebun?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect remove-data-from-delete-form"
                            data-dismiss="modal">Close
                    </button>
                    <button type="button" class="btn btn-danger waves-effect waves-light deleteRecord">Delete
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>

<!-- View Model -->
<form action="" method="POST" class="form-horizontal">
    <div id="view" data-backdrop="static" data-keyboard="false" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" style="width:100%;">
            <div class="modal-content" style="overflow: hidden;">
                <div class="modal-header">
                    <h4 class="modal-title" id="custom-width-modalLabel">Update</h4>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">×
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-6 col-form-label">Nama :</label>
                        <div class="col-sm-6">
                          <input type="text" readonly class="form-control-plaintext" value="{{ Auth::user()->name }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-6 col-form-label">Email :</label>
                        <div class="col-sm-6">
                          <input type="text" readonly class="form-control-plaintext" value="{{ Auth::user()->email }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-6 col-form-label">Kebun Terverifikasi pada :</label>
                        <div class="col-sm-6">
                          <input type="text" readonly class="form-control-plaintext" value="{{ Auth::user()->email_verified_at->format("d, F Y  H:i:A")}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-6 col-form-label">Dibuat pada :</label>
                        <div class="col-sm-6">
                          <input type="text" readonly class="form-control-plaintext" value="{{ Auth::user()->created_at->format("d, F Y  H:i:A") }}">
                        </div>
                    </div>
                    @if(Auth::user()->apikey !='nothing' || Auth::user()->email =='farizi2710@gmail.com' )
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-6 col-form-label">Status Kebun</label>
                            <div class="col-sm-6">
                            <input type="text" readonly class="form-control-plaintext" value="Aktif / terverifikasi" style="color : blue">
                            </div>
                        </div>
                    @else
                    <div class="form-group row">
                            <label for="staticEmail" class="col-sm-6 col-form-label">Status Kebun</label>
                            <div class="col-sm-6">
                            <input type="text" readonly class="form-control-plaintext" value="Tidak Aktif / terverifikasi" style="color : red">
                            </div>
                    </div>
                    @endif
                    
                <div class="modal-footer">
                    <button type="button" class="btn btn-light"
                            data-dismiss="modal">Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

