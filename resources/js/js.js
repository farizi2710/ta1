 // Initialize Firebase
 var config = {
    apiKey: "{{ config('services.firebase.api_key') }}",
    authDomain: "{{ config('services.firebase.auth_domain') }}",
    databaseURL: "{{ config('services.firebase.database_url') }}",
    storageBucket: "{{ config('services.firebase.storage_bucket') }}",
};
firebase.initializeApp(config);

var database = firebase.database();

var lastIndex = 0;

// Get Data
firebase.database().ref('kebun/').on('value', function (snapshot) {
    var value = snapshot.val();
    var htmls = [];
    $.each(value, function (index, value) {
        if (value) {
            htmls.push('<tr>\
            <td>' + value.name + '</td>\
            <td>' + value.email + '</td>\
            <td>' + value.suhu + '</td>\
            <td>' + value.tanah1 + '</td>\
            <td>' + value.tanah2 + '</td>\
            <td>' + value.tanah3 + '</td>\
            <td><button data-toggle="modal" data-target="#update-modal" class="btn btn-info updateData" data-id="' + index + '">Update</button>\
            <button data-toggle="modal" data-target="#remove-modal" class="btn btn-danger removeData" data-id="' + index + '">Delete</button></td>\
        </tr>');
        }
        lastIndex = index;
    });
    $('#tbody').html(htmls);
    $("#submitUser").removeClass('desabled');
});

// Add Data
$('#submitCustomer').on('click', function () {
    var values = $("#tambahkankebun").serializeArray();
    var name = values[0].value;
    var email = values[1].value;
    var suhu = values[2].value;
    var tanah1 = values[3].value;
    var tanah2 = values[4].value;
    var tanah3 = values[5].value;
    var userID = lastIndex + 1;

    console.log(values);

    firebase.database().ref('kebun/' + userID).set({
        name: name,
        email: email,
        suhu: suhu,
        tanah1: tanah1,
        tanah2: tanah2,
        tanah3: tanah3,
    });

    // Reassign lastID value
    lastIndex = userID;
    $("#tambahkankebun input").val("");
});

// Update Data
var updateID = 0;
$('body').on('click', '.updateData', function () {
    updateID = $(this).attr('data-id');
    firebase.database().ref('kebun/' + updateID).on('value', function (snapshot) {
        var values = snapshot.val();
        var updateData = '<div class="form-group">\
            <label for="first_name" class="col-md-12 col-form-label">Name</label>\
            <div class="col-md-12">\
                <input id="first_name" type="text" class="form-control" name="name" value="' + values.name + '" required autofocus>\
            </div>\
        </div>\
        <div class="form-group">\
            <label for="last_name" class="col-md-12 col-form-label">Email</label>\
            <div class="col-md-12">\
                <input id="last_name" type="text" class="form-control" name="email" value="' + values.email + '" required autofocus>\
            </div>\
        </div>\
        <div class="form-group">\
            <label for="suhu" class="col-md-12 col-form-label">suhu</label>\
            <div class="col-md-12">\
                <input id="suhu" type="text" class="form-control" name="suhu" value="' + values.suhu + '" required autofocus>\
            </div>\
        </div></div>\
        <div class="form-group">\
            <label for="tanah1" class="col-md-12 col-form-label">tanah1</label>\
            <div class="col-md-12">\
                <input id="tanah1" type="text" class="form-control" name="tanah1" value="' + values.tanah1 + '" required autofocus>\
            </div>\
        </div></div>\
        <div class="form-group">\
            <label for="tanah2" class="col-md-12 col-form-label">tanah2</label>\
            <div class="col-md-12">\
                <input id="tanah2" type="text" class="form-control" name="tanah2" value="' + values.tanah2 + '" required autofocus>\
            </div>\
        </div>\
        <div class="form-group">\
            <label for="tanah3" class="col-md-12 col-form-label">tanah3</label>\
            <div class="col-md-12">\
                <input id="tanah3" type="text" class="form-control" name="tanah3" value="' + values.tanah3 + '" required autofocus>\
            </div>\
        </div>';

        $('#updateBody').html(updateData);
    });
});

$('.update').on('click', function () {
    var values = $(".users-update-record-model").serializeArray();
    var postData = {
        name: values[0].value,
        email: values[1].value,
        suhu: values[2].value,
        tanah1: values[3].value,
        tanah2: values[4].value,
        tanah3: values[5].value,
    };

    var updates = {};
    updates['/kebun/' + updateID] = postData;

    firebase.database().ref().update(updates);

    $("#update-modal").modal('hide');
});

// Remove Data
$("body").on('click', '.removeData', function () {
    var id = $(this).attr('data-id');
    $('body').find('.users-remove-record-model').append('<input name="id" type="hidden" value="' + id + '">');
});

$('.deleteRecord').on('click', function () {
    var values = $(".users-remove-record-model").serializeArray();
    var id = values[0].value;
    firebase.database().ref('kebun/' + id).remove();
    $('body').find('.users-remove-record-model').find("input").remove();
    $("#remove-modal").modal('hide');
});
$('.remove-data-from-delete-form').click(function () {
    $('body').find('.users-remove-record-model').find("input").remove();
});